function switch_audio --wraps='bash ~/.config/leftwm/themes/current/scripts/switch_audio.sh' --description 'alias switch_audio bash ~/.config/leftwm/themes/current/scripts/switch_audio.sh'
  bash ~/.config/leftwm/themes/current/scripts/switch_audio.sh $argv; 
end
