# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# <<< conda initialize <<<
#
set -x MANPAGER "nvim -c 'set ft=man' -"

neofetch

export IHP_EDITOR="code --goto"

starship init fish | source

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/alexandre/Downloads/google-cloud-sdk/path.fish.inc' ]; . '/home/alexandre/Downloads/google-cloud-sdk/path.fish.inc'; end
